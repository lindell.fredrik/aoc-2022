import numpy as np


def is_tree_visible(tree_height_map, row_idx, col_idx):
    xy = tree_height_map[row_idx, col_idx]
    cols_before_xy = tree_height_map[row_idx, 0:col_idx]
    cols_after_xy = tree_height_map[row_idx, col_idx + 1 : np.shape(tree_height_map)[1]]
    rows_before_xy = tree_height_map[0:row_idx, col_idx]
    rows_after_xy = tree_height_map[row_idx + 1 : np.shape(tree_height_map)[1], col_idx]
    if (
        xy > max(cols_before_xy)
        or xy > max(cols_after_xy)
        or xy > max(rows_before_xy)
        or xy > max(rows_after_xy)
    ):
        return 1
    else:
        return 0


def get_number_of_visible_trees(trees, reference_height):
    nbr_of_visible_trees = 0
    for i, height in enumerate(trees):
        nbr_of_visible_trees += 1
        if height >= reference_height:
            break
    return 1 if nbr_of_visible_trees == 0 else nbr_of_visible_trees


def get_tree_senic_score(tree_height_map, row_idx, col_idx):
    xy = tree_height_map[row_idx, col_idx]
    cols_before_reversed_xy = tree_height_map[row_idx, 0:col_idx][::-1]
    nbr_of_visible_trees_left = get_number_of_visible_trees(cols_before_reversed_xy, xy)

    cols_after_xy = tree_height_map[row_idx, col_idx + 1 : np.shape(tree_height_map)[1]]
    nbr_of_visible_trees_right = get_number_of_visible_trees(cols_after_xy, xy)

    rows_before_reversed_xy = tree_height_map[0:row_idx, col_idx][::-1]
    nbr_of_visible_trees_up = get_number_of_visible_trees(rows_before_reversed_xy, xy)

    rows_after_xy = tree_height_map[row_idx + 1 : np.shape(tree_height_map)[1], col_idx]
    nbr_of_visible_trees_down = get_number_of_visible_trees(rows_after_xy, xy)

    return (
        nbr_of_visible_trees_left
        * nbr_of_visible_trees_right
        * nbr_of_visible_trees_up
        * nbr_of_visible_trees_down
    )


with open("08.input") as f:
    tree_height_map_input = list(
        map(lambda line: [int(number) for number in line], f.read().splitlines())
    )
    tree_height_map = np.array(tree_height_map_input)

    # part 1
    nbr_visible_trees = (
        np.shape(tree_height_map)[0] * 2 + (np.shape(tree_height_map)[1] - 2) * 2
    )
    # print("outer visible_trees:", nbr_visible_trees)
    for row_idx in range(1, np.shape(tree_height_map)[0] - 1):
        for col_idx in range(1, np.shape(tree_height_map)[1] - 1):
            nbr_visible_trees += is_tree_visible(tree_height_map, row_idx, col_idx)
    print("part 1:", nbr_visible_trees)

    # part 2
    highest_senic_score = 0
    for row_idx in range(1, np.shape(tree_height_map)[0] - 1):
        for col_idx in range(1, np.shape(tree_height_map)[1] - 1):
            tree_senic_score = get_tree_senic_score(tree_height_map, row_idx, col_idx)
            if tree_senic_score > highest_senic_score:
                highest_senic_score = tree_senic_score
    print("part 2:", highest_senic_score)
