NBR_OF_UNIQUE_MARKER = 4
NBR_OF_UNIQUE_MESSAGE = 14


def get_start_of_packet_marker(s):
    start = 0
    while len(set(s[start : start + NBR_OF_UNIQUE_MARKER])) != NBR_OF_UNIQUE_MARKER:
        start += 1
    return start + NBR_OF_UNIQUE_MARKER


def get_start_of_packet_message(s):
    start = 0
    while len(set(s[start : start + NBR_OF_UNIQUE_MESSAGE])) != NBR_OF_UNIQUE_MESSAGE:
        start += 1
    return start + NBR_OF_UNIQUE_MESSAGE


with open("06.input") as f:
    datasteam = f.read().rstrip("\n")
    print("part 1:", get_start_of_packet_marker(datasteam))
    print("part 2:", get_start_of_packet_message(datasteam))
