from collections import Counter

with open("03.input") as f:
    lower_case_letters = list(map(chr, range(ord("a"), ord("z") + 1)))
    upper_case_letters = list(map(chr, range(ord("A"), ord("Z") + 1)))
    letter_to_int_dict = {
        k: v + 1 for v, k in enumerate(lower_case_letters + upper_case_letters)
    }

    all_lines = f.read().rstrip("\n").split("\n")

    # Counter finds every char and nbr of occurrences
    # since unique values are used as priority and not together with nbr of occurrences we use keys()
    rucsack_comparment_common_item_unique_priorities = list(
        map(
            lambda x: letter_to_int_dict[
                list((Counter(x[: len(x) // 2]) & Counter(x[len(x) // 2 :])).keys())[0]
            ],
            all_lines,
        )
    )

    print("part 1:", sum(rucsack_comparment_common_item_unique_priorities))

    # split all lines into chunks of size 3
    elve_groups = [all_lines[i : i + 3] for i in range(0, len(all_lines), 3)]
    # find group badges within the chunks and convert to priority score
    elve_group_badge_pritorities = list(
        map(
            lambda x: letter_to_int_dict[
                list((Counter(x[0]) & Counter(x[1]) & Counter(x[2])).keys())[0]
            ],
            elve_groups,
        )
    )

    print("part 2:", sum(elve_group_badge_pritorities))
