#             [L] [M]         [M]
#         [D] [R] [Z]         [C] [L]
#         [C] [S] [T] [G]     [V] [M]
# [R]     [L] [Q] [B] [B]     [D] [F]
# [H] [B] [G] [D] [Q] [Z]     [T] [J]
# [M] [J] [H] [M] [P] [S] [V] [L] [N]
# [P] [C] [N] [T] [S] [F] [R] [G] [Q]
# [Z] [P] [S] [F] [F] [T] [N] [P] [W]
#  1   2   3   4   5   6   7   8   9
input_starting_stacks = {
    1: ["Z", "P", "M", "H", "R"],
    2: ["P", "C", "J", "B"],
    3: ["S", "N", "H", "G", "L", "C", "D"],
    4: ["F", "T", "M", "D", "Q", "S", "R", "L"],
    5: ["F", "S", "P", "Q", "B", "T", "Z", "M"],
    6: ["T", "F", "S", "Z", "B", "G"],
    7: ["N", "R", "V"],
    8: ["P", "G", "L", "T", "D", "V", "C", "M"],
    9: ["W", "Q", "N", "J", "F", "M", "L"],
}


def move_crate(n, f, t, invert=False):
    input_starting_stacks[t] = input_starting_stacks[t] + (
        input_starting_stacks[f][-n:][::-1] if invert else input_starting_stacks[f][-n:]
    )
    input_starting_stacks[f] = input_starting_stacks[f][:-n]


with open("05.input") as f:
    move_instructions = [
        line.split(" ") for line in f.read().split("\n\n")[1].rstrip("\n").split("\n")
    ]

    for line in move_instructions:
        # move_crate(int(line[1]), int(line[3]), int(line[5]), True) # part 1
        move_crate(int(line[1]), int(line[3]), int(line[5]))  # part 2

    message = ""
    for k, v in input_starting_stacks.items():
        message += v[-1:][0]
    print(message)
