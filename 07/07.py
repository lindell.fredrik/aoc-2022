with open("07.input") as f:
    stdout_list = [line.split(" ") for line in f.read().splitlines()]
    folder_with_size_dict = {}
    current_folder_hierarchy = []
    for line in stdout_list:
        if line[0] == "$":
            # print("input:", line)
            # user input
            if line[1] == "cd" and line[2] == "..":
                current_folder_hierarchy.pop()
                # print(current_folder_hierarchy)
            elif line[1] == "cd":
                current_folder_hierarchy.append(line[2])
                # print(current_folder_hierarchy)
        elif line[0].isnumeric():
            # print("file:", line)
            # will need the path
            current_folder_path = []
            for folder in current_folder_hierarchy:
                current_folder_path.append(folder)
                folder_with_size_dict[
                    "-".join(current_folder_path)
                ] = folder_with_size_dict.get("-".join(current_folder_path), 0) + int(
                    line[0]
                )

    # print(folder_with_size_dict)

    # part 1
    print(
        "part 1:",
        sum(
            [
                size
                for folder, size in folder_with_size_dict.items()
                if folder_with_size_dict[folder] <= 100000
            ]
        ),
    )

    # part 2
    total_space = 70000000
    free_space_needed = 30000000
    unused_space = total_space - folder_with_size_dict["/"]
    space_needed = free_space_needed - unused_space
    # print("unused_space:", unused_space)
    # print("space_needed:", space_needed)

    print(
        "part 2:",
        sorted(
            [
                size
                for folder, size in folder_with_size_dict.items()
                if folder_with_size_dict[folder] >= space_needed
            ]
        )[0],
    )
