from functools import reduce

with open("1.input") as f:
    elfs = [
        reduce(
            lambda x, y: x + y,
            list(map(int, line.split("\n"))),
        )
        for line in f.read().rstrip("\n").split("\n\n")
    ]

    # answer to part a
    print(sorted(elfs)[-1])

    # answer to part b
    print(reduce(lambda x, y: x + y, sorted(elfs)[-3:]))
