# A, X = Rock = 1
# B, Y = Paper = 2
# C, Z = Scissors = 3
hand_score = {"X": 1, "Y": 2, "Z": 3}
round_hand_score = {
    "A X": 3,
    "A Y": 6,
    "A Z": 0,
    "B X": 0,
    "B Y": 3,
    "B Z": 6,
    "C X": 6,
    "C Y": 0,
    "C Z": 3,
}

# A = Rock = 1
# B = Paper = 2
# C = Scissors = 3
# X = Loss
# Y = Draw
# Z = Win
outcome_score = {"X": 0, "Y": 3, "Z": 6}
round_outcome_score = {
    "A X": 3,
    "A Y": 1,
    "A Z": 2,
    "B X": 1,
    "B Y": 2,
    "B Z": 3,
    "C X": 2,
    "C Y": 3,
    "C Z": 1,
}

with open("2.input") as f:
    all_lines = f.read().rstrip("\n").split("\n")

    # part one
    round_hand_score_list = list(
        map(
            lambda x: round_hand_score[x] + hand_score[x.split(" ")[1]],
            all_lines,
        )
    )
    print("part 1:", sum(round_hand_score_list))

    # part two
    round_outcome_score_list = list(
        map(
            lambda x: round_outcome_score[x] + outcome_score[x.split(" ")[1]],
            all_lines,
        )
    )
    print("part 2:", sum(round_outcome_score_list))
