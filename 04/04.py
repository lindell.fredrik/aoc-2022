with open("04.input") as f:
    all_lines = f.read().rstrip("\n").split("\n")

    # convert row to number ranges
    # x is range in text e.g. '2-4'
    elf_range_id_list = list(
        map(
            lambda x: [
                list(range(int(x[0].split("-")[0]), int(x[0].split("-")[1]) + 1)),
                list(range(int(x[1].split("-")[0]), int(x[1].split("-")[1]) + 1)),
            ],
            list(map(lambda x: x.split(","), all_lines)),
        )
    )

    # part one, find fully overlapping count
    overlaps = list(
        filter(
            lambda x: x is True,
            list(
                map(
                    lambda row: len(set(row[0]).intersection(row[1])) == len(row[0])
                    or len(set(row[0]).intersection(row[1])) == len(row[1]),
                    elf_range_id_list,
                )
            ),
        )
    )
    print("part 1:", len(overlaps))

    # part two, find all overlapping ranges
    overlapping_ranges = list(
        filter(
            lambda x: len(x) > 0,
            list(map(lambda row: set(row[0]).intersection(row[1]), elf_range_id_list)),
        )
    )
    print("part 2:", len(overlapping_ranges))
